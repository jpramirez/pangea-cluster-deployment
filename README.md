# deployments nuevos

## pasos

1. hacer una copia del archivo deploymentIntegraciones.yaml y nombarlo de acuerdo al deployment que se realizara.
2. si no existen previamente los pv y pvc de nombre integraciones-data, crearlos ejecutando:

```shell
kubectl create -f pv-integraciones.yaml
kubectl create -f pvc-integraciones.yaml
```

3. copiar el archivo .jar al folder /home/nfs/nfs/integraciones-data/nombredeldeployment/archivo.jat
4. editar el archivo de deployment creado en el paso 1 y localizar la siguiente linea:

```yaml
        command: ["/usr/bin/java"]
        args: ["-jar", "/opt/local/anm.cu001/anm.cu001.preciobasemineral.connector-jar-with-dependencies.jar"]
```

> Reemplazar la ruta establecida en __args__ para que apunte al folder y archivo jar correctos
> Se debe tener en cuenta que siempre la ruta base sera `/opt/local`.
> reemplazar todas las referecias de `name` y `component` que se refieran a `integraciones` por
> el nombre que se le quiera asignar en el deployment

5. crear  deployment

```shell
kubectl create -f deploymentNombredelDeployment.yaml
```
